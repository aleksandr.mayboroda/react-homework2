import React, { PureComponent } from "react";
import "./Modal.scss";

import PropTypes from "prop-types";

class Modal extends PureComponent {

  closeModalByClickOutside = (ev) => {
    if (!ev.target.closest(".modal_window")) {
      this.props.handler();
    }
  };

  render() {
    const { header, text, actions, closeButton, handler } = this.props;
    return (
      <div className="modal" onClick={this.closeModalByClickOutside}>
        <div className="modal_window">
          {closeButton && (
            <span className="modal_close" onClick={handler}></span>
          )}
          <div className="modal_header">{header}</div>
          <div className="modal_content">{text}</div>
          {actions && <div className="modal_footer">{actions}</div>}
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  header: PropTypes.string.isRequired,
  text: PropTypes.object.isRequired,
  actions: PropTypes.object,
  closeButton: PropTypes.bool,
  handler: PropTypes.func.isRequired,
};

Modal.defaultProps = {
  actions: {},
  closeButton: false,
}

export default Modal;
