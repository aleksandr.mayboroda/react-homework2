import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import "./style.scss";

class Cart extends PureComponent {
  render() {
    const { counter, sum } = this.props;
    return (
      <div className="cart">
        <div className="cart__text">
          <span>
            You have <span className="cart__counter">{counter}</span>{" "}
            {counter > 1 ? "products" : "product"} in cart
          </span>
          {sum > 0 && (
            <>
              {" "}
              for <span className="cart__sum"> {sum}$</span>
            </>
          )}
        </div>
        <img src="/cart.png" alt="cart__image" className="cart__image" />
      </div>
    );
  }
}

Cart.propTypes = {
  counter: PropTypes.number.isRequired,
  sum: PropTypes.number.isRequired,
};

export default Cart;
