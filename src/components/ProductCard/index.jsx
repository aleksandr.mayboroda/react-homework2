import React, { PureComponent } from "react";
// import React, { useEffect } from "react";
import "./style.scss";
import PropTypes from 'prop-types'
import Button from "../Button";
import Favorite from "../Favorite";
import Modal from "../Modal";

class ProductCard extends PureComponent {
  state = {
    isOpenedModal: false,
  };

  modalToggle = () => {
    this.setState({
      isOpenedModal: !this.state.isOpenedModal,
    });
  };
 
  render() {
    const { name, price = 0, imagePath, articul, color } = this.props.product;
    const {cartHandler, isInCart, favoriteHandler, isInFavorites} = this.props
    // console.log('isInFavorites',isInFavorites)
    const { isOpenedModal } = this.state;
    return (
      <div className="product">
        <div className="product__image">
          <img src={imagePath} alt={name} />
        </div>
        <div className="product__info">
          <h3 className="product__name">{name}</h3>

          <p className="product__articul">articul: {articul}</p>
          <p className="product__price">{price} $</p>
          <p className="product__color">color: <span style={{color}}>{color}</span></p>
          <div className="product__actions">
            <Favorite
              func={favoriteHandler}
              art={articul}
              filled={isInFavorites}
            />
            <Button
              text={isInCart ? "In cart" : "Add to cart"}
              className={isInCart ? "btn-yellow" : "btn-water"}
              func={this.modalToggle}
            />
            {isOpenedModal && (
              <Modal
                header={"Warning"}
                text={isInCart ? <p>Are you shure you want to remove "{name}" from cart?</p> : <p>Are you shure you want to add "{name}" to cart?</p>}
                handler={this.modalToggle}
                actions={
                  <>
                    <Button
                      backgroundColor={"#b3382c"}
                      func={() => {
                        cartHandler({articul,price,quantity: 1})
                        this.modalToggle()
                      }}
                      text={isInCart ? "Remove" : "Add"}
                    />
                    <Button
                      backgroundColor={"#b3382c"}
                      func={this.modalToggle}
                      text={"Cancel"}
                    />
                  </>
                }
              />
            )}
          </div>
        </div>
      </div>
    );
  }
}

ProductCard.propTypes = {
  product: PropTypes.exact({
    name: PropTypes.string.isRequired,
    price: PropTypes.number,
    imagePath: PropTypes.string,
    articul: PropTypes.string,
    color: PropTypes.string,
  }),
  cartHandler: PropTypes.func.isRequired,
  isInCart: PropTypes.bool,
  favoriteHandler: PropTypes.func.isRequired,
  isInFavorites: PropTypes.bool,
}


// const ProductCard = ({ product }) => {
//   const { name, price, imagePath, articul, color } = product;
//   return (
//     <div className="product">
//       <div className="product__image">
//         <img src={imagePath} alt={name} />
//       </div>
//       <div className="product__info">
//         <h3 className="product__name">{name}</h3>

//         <p className="product__articul">Articul: {articul}</p>
//         <p className="product__price">{price} $</p>
//         <p className="product__color">{color}</p>
//         <div className="product_actions">
//           <Button bg="blue" text="Add to cart" className = {'btn-water'} func = {() => console.log('sss')}/>
//         </div>
//       </div>
//     </div>
//   );
// };

export default ProductCard;
