import React from "react";
import PropTypes from "prop-types";

import * as Icons from "../../theme/icons";

function Icon({ type, className = '', ...rest }) {
  const iconJsx = Icons[type];

  if (!iconJsx) {
    return null;
  }

  return (
    <span className={`icon icon-type ${className}`}>
      {iconJsx({ ...rest })}
    </span>
  );
}

Icon.propTypes = {
  type: PropTypes.string.isRequired,
  className: PropTypes.string,
};

export default Icon;
