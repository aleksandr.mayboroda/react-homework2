import React from "react";
import "./style.scss";
import PropTypes from "prop-types";

function Button({ text, func, className = "" }) {
  return (
    <button className={`btn ${className}`} onClick={func}>
      {text}
    </button>
  );
}

Button.propTypes = {
  text: PropTypes.string.isRequired,
  func: PropTypes.func.isRequired,
  className: PropTypes.string,
};

Button.defaultProps = {
  className: "",
};

export default Button;
